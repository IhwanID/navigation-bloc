import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutternavbloc/bloc/a/a_bloc.dart';
import 'package:flutternavbloc/bloc/a/a_event.dart';
import 'package:flutternavbloc/bloc/a/a_state.dart';

void main() {
  runApp(MultiBlocProvider(providers: [
    BlocProvider(
      create: (context) => ABloc(),
      child: MyApp(),
    ),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocBuilder<ABloc, AState>(builder: (_, state) {
        if (state is InitialAState) {
          return HomePage();
        } else {
          return AnotherPage();
        }
      }),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomePage'),
      ),
      body: BlocListener<ABloc, AState>(
        listener: (context, state) {
          if (state is LoadFailureAState) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AnotherPage()));
          }
        },
        child: Center(
          child: RaisedButton(
            child: Text('Go to Second Page'),
            onPressed: () {
              BlocProvider.of<ABloc>(context).add(SecondEvent());
            },
          ),
        ),
      ),
    );
  }
}

class AnotherPage extends StatefulWidget {
  @override
  _AnotherPageState createState() => _AnotherPageState();
}

class _AnotherPageState extends State<AnotherPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Go to First Page'),
          onPressed: () {
            BlocProvider.of<ABloc>(context).add(FirstEvent());
          },
        ),
      ),
    );
  }
}
