import 'package:equatable/equatable.dart';

abstract class BState extends Equatable {
  const BState();
}

class InitialBState extends BState {
  @override
  List<Object> get props => [];
}
