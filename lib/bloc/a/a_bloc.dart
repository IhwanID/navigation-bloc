import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class ABloc extends Bloc<AEvent, AState> {
  @override
  AState get initialState => InitialAState();

  @override
  Stream<AState> mapEventToState(
    AEvent event,
  ) async* {
    if (event is FirstEvent) {
      yield InitialAState();
    } else if (event is SecondEvent) {
      yield LoadFailureAState();
      // yield LoadSuccessAState();
    }
  }
}
