import 'package:equatable/equatable.dart';

abstract class AState extends Equatable {
  const AState();
}

class InitialAState extends AState {
  @override
  List<Object> get props => [];
}

class LoadInProgressAState extends AState {
  @override
  List<Object> get props => [];
}

class LoadSuccessAState extends AState {
  @override
  List<Object> get props => [];
}

class LoadFailureAState extends AState {
  @override
  List<Object> get props => [];
}
