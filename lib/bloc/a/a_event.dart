import 'package:equatable/equatable.dart';

abstract class AEvent extends Equatable {
  const AEvent();
  @override
  List<Object> get props => [];
}

class FirstEvent extends AEvent {
  const FirstEvent();
}

class SecondEvent extends AEvent {
  const SecondEvent();
}
